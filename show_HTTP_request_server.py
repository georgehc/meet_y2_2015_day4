from flask import Flask
from flask import request
from flask import render_template
app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def print_HTTP_request():
    # displays HTTP request information in the terminal
    print("\n\n" +
          str(request.headers) + "\n".join(request.get_data().split("&")))
    return render_template('form_demo.html')


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5000)
